var _reviewer_script=function(){
var tabs=$.fn.dataTable.fnTables()
var dt1=$(tabs[0]).dataTable()
var dt2=$(tabs[1]).dataTable()
var recalculate=function(dt){
	$(dt.fnGetData()).each(function(i) {
            var n=function(t) {return parseFloat(dt.fnGetData(i,t))};
            dt.fnUpdate((n(3)+n(4)*0.1-n(5)*0.1-n(6)).toFixed(1), i, 7) 
       })
}
//recalculate(dt1)
//recalculate(dt2)
$("table").find("a").on("click", function(e){
  e.preventDefault();
  var win=window.open($(this).attr("href"), 'review_window');
  win.focus();
})
var newdiv=$("<div>")
var yourid=$("#drop1").find("b").html()
newdiv.load("http://tw.pycon.org/2013/zh/review/list/"+yourid+"/ table", function(){
var dict={}
newdiv.find("tr").find(".title").find("b").each(function(i) { dict[$(this).html()]=1})
$("tr").each(function(i){var id=$(this).find(".title").find("b").html(); if(id in dict) $(this).css('background', '#99bb99') })
}
)
var newdiv2=$("<div>")
newdiv2.load("http://tw.pycon.org/2013/zh/review/list/ table", function(){
var dict2={}
newdiv2.find("tr").each(function () {
	var id=$(this).find(".title").find("b").html()
        var x=$(this).find("td"); 
        var n=function(t) { return parseFloat($(x[t]).html())};
	dict2[id]=[n(3), n(4), n(5), n(6)]
}
)

var updateData=function(dt){
	var arr=$(dt.fnGetData())
        arr.each(function(i) {
            var n=function(t) {return parseFloat(arr[i][t])};
	    var id=$(arr[i][0]).find("b").html()	 
	    var total=0.0  
	    if(id in dict2){
		var xx=dict2[id]
		for(var j=0;j<4;j++){
			if(n(j+3) != xx[j])
				dt.fnUpdate(xx[j], i, j+3)
		}
		total=xx[0]+(xx[1]-xx[2])*0.1-xx[3]
	    }
	    else{
	        total=n(3)+n(4)*0.1-n(5)*0.1-n(6)
	    }
	    if(n(7)!=total)
            	dt.fnUpdate(total.toFixed(1), i, 7) 
	    
       })
}
updateData(dt1)
updateData(dt2)
// dt1.fnDraw()
// dt2.fnDraw()
})

}
jQuery.getScript("http://www.datatables.net/download/build/jquery.dataTables.min.js", 
	function(){
		$("table").dataTable({"bLengthChange": false, "bPaginate": false})
		$("input[aria-controls]").val("undecided").keyup()
		_reviewer_script()
		$(window).focus(_reviewer_script)
	}
)
